# Eigensolvers for Pyspark Comparison

The objective of this notebook is to compare various methods of computing the eigenvalues and eigenvalues of an adjacency matrix. The eigenvalues and eigenvectors are crucial parts of EDA in graph theory. For example, the eigenvalues and eigenvectors can be used to determine different centrality measures within the graph, such as degree centrality, betweenness centrality, and eigenvector centrality. Additionally, they can help to identify important structural features of the graph, such as connected components and communities. In this notebook, we will explore several methods for computing the eigenvalues and eigenvectors of an adjacency matrix starting with numpy and scipy methods for small graphs, as well as exploring methods of implementing PySpark to scale the computation on large graphs.

The following code is essentially setting up an environment to work with a graph that is stored in a Neo4j database.

It establishes a connection to the database, reads the vertex and edge information, and creates a graph object using GraphFrame. GraphFrame is a package that extends Apache Spark with graph computation capabilities, allowing for graph computations to be done in a distributed manner. The graph object can then be used for various graph computations, such as computing centrality measures or detecting communities within the graph.

For the sake of speed, we will be importing a quite small graph (179 nodes), from a Neo4j Server loaded with the classic Movies example.