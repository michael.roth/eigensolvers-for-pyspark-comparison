import time
from pyspark import SparkConf, SparkContext
import random
import matplotlib.pyplot as plt
import numpy as np

######## Utilities ########

def progress_bar(i, total, print_length=40, prefix=''):
    progress_ratio = i / total
    progress_length = int(progress_ratio * print_length)
    progress_string = prefix + ' <' + '=' * progress_length + '-' * (print_length - progress_length) + '> ' + "{:.2f}".format(progress_ratio * 100) +'%' + ' '*4
    print(progress_string, end='\r')

# Function for dot product between matrix (j, (i, value)) and vector (i, value)
def matrix_vector_dot_product(matrix_rdd, vector_rdd, num_partitions):
    dot_product_rdd = matrix_rdd.map(lambda x: (x[1][0], (x[0], x[1][1]))).join(vector_rdd) \
                                .map(lambda x: (x[1][0][0], x[1][0][1] * x[1][1])) \
                                .reduceByKey(lambda x, y: x + y).repartition(num_partitions)
    return dot_product_rdd

# Function for dot product between vector and vector (i, value)
def vector_vector_dot_product(vector1_rdd, vector2_rdd, num_partitions):
    dot_product_rdd = vector1_rdd.join(vector2_rdd) \
                                .mapValues(lambda x: x[0]*x[1]) \
                                .map(lambda x: (0, x[1])) \
                                .reduceByKey(lambda x, y: x+y).repartition(num_partitions)
    return dot_product_rdd

def identity_rdd(n, sc):
    return sc.parallelize([(i, (i, 1)) for i in range(n)])

def euclidean_norm(vector_rdd):
    return vector_rdd.map(lambda x: x[1]*x[1]).reduce(lambda x, y: x+y)**.5

# Function to normalize a vector based on its Euclidean norm
def normalize_vector(vector_rdd):
    norm = euclidean_norm(vector_rdd)
    return vector_rdd.mapValues(lambda x: x / norm)

def get_n(rdd):
    return rdd.filter(lambda x: x[0]).reduce(lambda a,b: max(a,b))[0] + 1

def matrix_matrix_dot_product(matrix1_rdd, matrix2_rdd, num_partitions):
    # Rearrange matrix1 to the format (j, (i, value1))
    matrix1_rdd = matrix1_rdd.map(lambda x: (x[1][0], (x[0], x[1][1])))
    
    # Join matrix1 and matrix2 on the common key 
    # Compute the product of value1 and value2 and rearrange to the format ((i, j), value1 * value2)
    # Sum the products for each unique (i, j) pair
    # Map the final matrix to (i, (j, value))
    dot_product_rdd = matrix1_rdd.join(matrix2_rdd) \
                            .map(lambda x: ((x[1][0][0], x[1][1][0]), x[1][0][1] * x[1][1][1])) \
                            .reduceByKey(lambda x, y: x + y) \
                            .map(lambda x: (x[0][0], (x[0][1], x[1]))) \
                            .repartition(num_partitions)

    return dot_product_rdd

def random_symmetric_tridiagonal_matrix(sc, n):
    # Create a list to store the matrix entries
    entries = []

    for i in range(n):
        # Diagonal elements
        value = random.random()
        entries.append((i, (i, value)))

        # Upper diagonal elements
        if i < n-1:
            value = random.random()
            entries.append((i, (i+1, value)))
            entries.append((i+1, (i, value)))  # Symmetric lower diagonal elements

    # Create an RDD from the list of entries
    matrix_rdd = sc.parallelize(entries)

    return matrix_rdd

def random_symmetric_matrix(sc, n, density):
    # Create a list to store the matrix entries
    entries = []

    for i in range(n):
        for j in range(i, n):
            # Diagonal elements
            if i == j:
                value = random.random()
                entries.append((i, (j, value)))
            # Off-diagonal elements
            elif random.random() < density:
                value = random.random()
                entries.append((i, (j, value)))
                entries.append((j, (i, value)))  # Symmetric elements

    # Create an RDD from the list of entries
    matrix_rdd = sc.parallelize(entries)

    return matrix_rdd

def random_laplacian_matrix(sc, n, density):
    # Create a list to store the matrix entries
    entries = []

    for i in range(n):
        degree = 0
        for j in range(n):
            if i == j:
                continue  # Skip diagonal elements for now
            
            # Off-diagonal elements
            if random.random() < density:
                value = -1
                degree += abs(value)
                entries.append((i, (j, value)))
                entries.append((j, (i, value)))  # Symmetric elements

        # Diagonal elements
        entries.append((i, (i, degree)))

    # Create an RDD from the list of entries
    matrix_rdd = sc.parallelize(entries)

    return matrix_rdd

######## LANCZOS ########

# Lanczos Method implementation
def tridiagonalize_lanczos(matrix_rdd, num_partitions, k=10, v_i_rdd=None):
    
    n = get_n(matrix_rdd)
    
    sc = matrix_rdd.context
    
    if v_i_rdd == None:
        q_rdd = sc.parallelize(np.random.rand(n)).zipWithIndex().map(lambda x: (x[1], x[0])).repartition(num_partitions)
        v_i_rdd = normalize_vector(q_rdd).repartition(num_partitions)
    
    alpha, beta = [], []

    start_time = time.time()
    iterations = min(k, n)
    for i in range(iterations):
        w_i_rdd = matrix_vector_dot_product(matrix_rdd, v_i_rdd, num_partitions)
        alpha_i = vector_vector_dot_product(v_i_rdd, w_i_rdd, num_partitions).reduce(lambda x, y: x+y)[1]
        w_i_rdd = w_i_rdd.join(v_i_rdd).mapValues(lambda x: x[0] - (x[1] * alpha_i)).repartition(num_partitions)

        if i > 0:
            w_i_rdd = w_i_rdd.join(v_im1_rdd).mapValues(lambda x: x[0] - (x[1] * beta_i)).repartition(num_partitions)
            
        beta_i = euclidean_norm(w_i_rdd)
        if beta_i == 0:
            break

        alpha.append(alpha_i)
        beta.append(beta_i)
        
        v_im1_rdd = v_i_rdd
        v_i_rdd = w_i_rdd.mapValues(lambda x: x / beta_i)
        
        # time and progress
        total_time = time.time() - start_time
        avg_iteration = total_time / (i+1)
        time_left = avg_iteration * (iterations- i) /60
        progress_bar(i+1, iterations, prefix=f'step:{i+1}, Avg. Iteration: {avg_iteration:.2f} s, Time Left: {time_left:.2f} min.')
        
        if i == 0:
            lanczos_vectors_rdd = v_i_rdd.map(lambda x: (0, (x[0], x[1])))
            
        else:
            lanczos_vectors_rdd = lanczos_vectors_rdd.union(v_i_rdd.map(lambda x: (i, (x[0], x[1]))))
                    
    tridiagonal_data = sc.parallelize(zip(range(k), alpha , beta)) \
                            .flatMap(lambda x: [(x[0], x[0], x[1]), (x[0], x[0] + 1, x[2]), (x[0] + 1, x[0], x[2])])
    tridiagonal_rdd = tridiagonal_data.filter(lambda x: x[0] < k and x[1] < k and x[2] != 0).map(lambda x: (x[0], (x[1], x[2])))

    return tridiagonal_rdd, lanczos_vectors_rdd

def rdd_to_numpy(matrix_rdd, n_i=None, n_j=None):
    
    if n_i == None:
        n_i = get_n(matrix_rdd)
    if n_j == None:
        n_j = n_i
    
    np_array = np.zeros((n_i,n_j))
    for i, (j, value) in matrix_rdd.collect():
        np_array[i][j] = value
        
    return np_array

def numpy_to_rdd(np_matrix, sc):
    
    rdd_array = []
    
    shape_len = len(np_matrix.shape)
    
    if shape_len == 1:
        for i in range(np_matrix.shape[0]):
            rdd_array.append((i, np_matrix[i]))
    
    elif shape_len == 2:
        for i in range(np_matrix.shape[0]):
            for j in range(np_matrix.shape[1]):
                rdd_array.append((i,(j, np_matrix[i][j])))
                
        
    else:
        raise Exception(f"numpy matrix must be either shape length 1 or 2. This one has shape {np_matrix.shape}")
    
    matrix_rdd = sc.parallelize(rdd_array)
    
    return matrix_rdd

def eig_rdd(matrix_rdd, num_partitions, k=10, num_restarts=1):
    
    """ Eigensolver for Pyspark RDD's """
    
    from scipy.linalg import eig
    
    sc = matrix_rdd.context

    for r in range(num_restarts):
        print('\nRestart', r, '\n')
        if r == 0:
            T_rdd, lanczos_rdd = tridiagonalize_lanczos(matrix_rdd, num_partitions=num_partitions, k=k)
        else:
            T_rdd, lanczos_rdd = tridiagonalize_lanczos(matrix_rdd, num_partitions=num_partitions, k=k, v_i_rdd=v_i_rdd)

        T = rdd_to_numpy(T_rdd)

        eigvals_T, eigvecs_T = eig(T)

        eigvals_T, eigvecs_T = eigvals_T.real, eigvecs_T.real

        index_map = {k: v for k, v in zip(np.argsort(eigvals_T), range(len(eigvals_T)))}

        eigvecs_T_rdd = numpy_to_rdd(eigvecs_T, sc)

        eigvals_A_rdd = numpy_to_rdd(eigvals_T, sc)

        lanczos_rdd = lanczos_rdd.map(lambda x: (x[1][0], (x[0], x[1][1])))

        eigvecs_A_rdd = matrix_matrix_dot_product(lanczos_rdd, eigvecs_T_rdd, num_partitions) \
                        .mapValues(lambda x: (index_map[x[0]], x[1]))
        
        v_i_rdd = eigvecs_A_rdd.filter(lambda x: x[1][0] == 0).map(lambda x: (x[0], x[1][1]))
    
    return eigvals_A_rdd, eigvecs_A_rdd
        